package org.idea.mq.redis.framework.config;

import org.idea.mq.redis.framework.bean.HandlerResult;
import redis.clients.jedis.StreamEntryID;

/**
 * @Author linhao
 * @Date created in 10:36 下午 2022/2/9
 */
public class MQConstants {

    public static final HandlerResult SUCCESS = new HandlerResult();
    public static final HandlerResult CONSUME_FAIL = new HandlerResult();

    public static final StreamEntryID BEGIN_ENTRY = new StreamEntryID() {

        private static final long serialVersionUID = 1L;

        @Override
        public String toString() {
            return "0-0";
        }
    };
}
