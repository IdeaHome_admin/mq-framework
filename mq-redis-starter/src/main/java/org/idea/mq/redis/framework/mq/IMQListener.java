package org.idea.mq.redis.framework.mq;

import org.idea.mq.redis.framework.bean.MsgWrapper;

/**
 * mq的基本监听端
 *
 * @Author linhao
 * @Date created in 3:18 下午 2022/2/7
 */
public interface IMQListener {

    /**
     * 当消息触发
     *
     * @param msgWrapper
     */
    void onMessageReach(MsgWrapper msgWrapper);
}
