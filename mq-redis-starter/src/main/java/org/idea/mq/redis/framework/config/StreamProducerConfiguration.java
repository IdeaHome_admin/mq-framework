package org.idea.mq.redis.framework.config;

import org.idea.mq.redis.framework.producer.IStreamProducer;
import org.idea.mq.redis.framework.producer.StreamProducer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

/**
 * @Author linhao
 * @Date created in 3:52 下午 2022/2/10
 */
public class StreamProducerConfiguration{

    @ConditionalOnMissingBean
    @Bean
    public IStreamProducer iStreamProducer(){
        return new StreamProducer();
    }
}
