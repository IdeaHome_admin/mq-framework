package org.idea.mq.redis.framework.listener;

import org.idea.mq.redis.framework.bean.HandlerResult;
import org.idea.mq.redis.framework.config.StreamListener;
import org.idea.mq.redis.framework.mq.xstream.RedisStreamMQListener;
import redis.clients.jedis.StreamEntry;

/**
 * @Author linhao
 * @Date created in 2:53 下午 2022/2/10
 */
@StreamListener(streamName = "order-service:order-payed-topic", groupName = "user-service-group", consumerName = "user-service-consumer")
public class UserUpgradeListener implements RedisStreamMQListener {

    @Override
    public HandlerResult handleMsg(StreamEntry streamEntry) {
        return null;
    }
}
