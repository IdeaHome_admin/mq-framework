package org.idea.mq.redis.framework.mq;

import org.idea.mq.redis.framework.bean.MsgWrapper;

/**
 * 基本的mq接口
 *
 * @Author linhao
 * @Date created in 3:07 下午 2022/2/7
 */
public interface IMQTemplate {

    boolean send(MsgWrapper msgWrapper);
}
