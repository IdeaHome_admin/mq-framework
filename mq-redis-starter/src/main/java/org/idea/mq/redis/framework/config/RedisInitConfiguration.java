package org.idea.mq.redis.framework.config;

import org.idea.mq.redis.framework.redis.IRedisService;
import org.idea.mq.redis.framework.redis.IRedisServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @Author linhao
 * @Date created in 6:47 下午 2021/4/30
 */
@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisInitConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(RedisInitConfiguration.class);

    @Resource
    private RedisProperties redisProperties;

    @Bean
    @ConditionalOnMissingBean
    public IRedisFactory iRedisFactory() {
        return new IRedisFactoryImpl(redisProperties);
    }

    @Bean
    @ConditionalOnBean(value = IRedisFactory.class)
    public IRedisService iRedisService(@Qualifier("iRedisFactory") IRedisFactory iRedisFactory) {
        LOGGER.info("##### RedisInitConfiguration init iRedisService #####");
        IRedisServiceImpl iRedisService = new IRedisServiceImpl();
        iRedisService.setIRedisFactory(iRedisFactory);
        return iRedisService;
    }

}
