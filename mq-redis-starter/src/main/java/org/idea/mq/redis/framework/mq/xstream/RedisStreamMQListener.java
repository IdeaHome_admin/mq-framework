package org.idea.mq.redis.framework.mq.xstream;


import org.idea.mq.redis.framework.bean.HandlerResult;
import redis.clients.jedis.StreamEntry;

/**
 * @Author linhao
 * @Date created in 10:08 下午 2022/2/9
 */
public interface RedisStreamMQListener {


    /**
     * 处理正常消息
     */
    HandlerResult handleMsg(StreamEntry streamEntry);

}
