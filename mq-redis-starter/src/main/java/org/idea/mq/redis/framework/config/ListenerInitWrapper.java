package org.idea.mq.redis.framework.config;

/**
 * @Author linhao
 * @Date created in 12:00 下午 2022/2/10
 */
public class ListenerInitWrapper {

    private String streamName;

    private String groupName;

    private String consumerName;


    public ListenerInitWrapper(String streamName, String groupName, String consumerName) {
        this.streamName = streamName;
        this.groupName = groupName;
        this.consumerName = consumerName;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getConsumerName() {
        return consumerName;
    }

    public void setConsumerName(String consumerName) {
        this.consumerName = consumerName;
    }
}
