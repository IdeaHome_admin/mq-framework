package org.idea.mq.redis.framework.producer;


import org.idea.mq.redis.framework.redis.IRedisService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


/**
 * @Author linhao
 * @Date created in 12:19 下午 2022/2/10
 */
public class StreamProducer implements IStreamProducer{

    @Resource
    private IRedisService iRedisService;

    @Override
    public void sendMsg(String streamName,String json){
        Map<String,String> map = new HashMap<>();
        map.put("json",json);
        iRedisService.xAdd(streamName,map);
    }

}
