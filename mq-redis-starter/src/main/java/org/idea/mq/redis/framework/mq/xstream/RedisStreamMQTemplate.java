package org.idea.mq.redis.framework.mq.xstream;

import org.idea.mq.redis.framework.bean.MsgWrapper;
import org.idea.mq.redis.framework.mq.IMQTemplate;
import org.idea.mq.redis.framework.redis.IRedisService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author linhao
 * @Date created in 9:40 上午 2022/2/8
 */
@Component
public class RedisStreamMQTemplate implements IMQTemplate {

    @Resource
    private IRedisService iRedisService;

    @Override
    public boolean send(MsgWrapper msgWrapper) {
        iRedisService.xAdd(msgWrapper.getTopic(),  msgWrapper.getKeyMap());
        return true;
    }

}
