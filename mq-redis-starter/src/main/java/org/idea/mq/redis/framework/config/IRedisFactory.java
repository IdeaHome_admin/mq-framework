package org.idea.mq.redis.framework.config;

import redis.clients.jedis.Jedis;

/**
 * 只处理jedis的创建，销毁工作
 *
 * @author idea
 * @data 2020/4/1
 */
public interface IRedisFactory {

    /**
     * 获取链接
     *
     * @return
     */
    Jedis getConnection();

    /**
     * 关闭
     *
     * @return
     */
    Boolean close(Jedis jedis);
}
