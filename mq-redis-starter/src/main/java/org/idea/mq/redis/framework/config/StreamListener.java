package org.idea.mq.redis.framework.config;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @Author linhao
 * @Date created in 10:04 下午 2022/2/9
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface StreamListener {

    String streamName() default "";

    String groupName() default "";

    String consumerName() default "";

}
