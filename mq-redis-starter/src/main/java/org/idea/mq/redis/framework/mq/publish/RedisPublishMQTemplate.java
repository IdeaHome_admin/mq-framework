package org.idea.mq.redis.framework.mq.publish;

import com.alibaba.fastjson.JSON;
import org.idea.mq.redis.framework.bean.MsgWrapper;
import org.idea.mq.redis.framework.mq.IMQTemplate;
import org.idea.mq.redis.framework.redis.IRedisService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 基于redis的发布订阅模式来进行实现，但是对于消息的可靠性不高，容易出现丢失情况
 *
 * @Author linhao
 * @Date created in 4:48 下午 2022/2/7
 */
@Component
public class RedisPublishMQTemplate implements IMQTemplate {

    @Resource
    private IRedisService iRedisService;

    @Override
    public boolean send(MsgWrapper msgWrapper) {
        String content = JSON.toJSONString(msgWrapper.getMsgInfo());
        iRedisService.publish(msgWrapper.getTopic(),content);
        return true;
    }
}
