package org.idea.mq.redis.framework.bean;

import java.util.List;
import java.util.Map;

/**
 * @Author linhao
 * @Date created in 11:47 上午 2022/2/8
 */
public class MsgWrapper {

    private List<String> msgInfo;

    private Map<String,String> keyMap;

    private String topic;

    public MsgWrapper() {
    }

    public MsgWrapper(String topic) {
        this.topic = topic;
    }

    public List<String> getMsgInfo() {
        return msgInfo;
    }

    public void setMsgInfo(List<String> msgInfo) {
        this.msgInfo = msgInfo;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, String> getKeyMap() {
        return keyMap;
    }

    public void setKeyMap(Map<String, String> keyMap) {
        this.keyMap = keyMap;
    }

    @Override
    public String toString() {
        return "MsgWrapper{" +
                "msgInfo=" + msgInfo +
                ", topic='" + topic + '\'' +
                '}';
    }
}
