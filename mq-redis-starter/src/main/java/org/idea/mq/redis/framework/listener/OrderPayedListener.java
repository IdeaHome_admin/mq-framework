package org.idea.mq.redis.framework.listener;

import org.idea.mq.redis.framework.bean.HandlerResult;
import org.idea.mq.redis.framework.config.StreamListener;
import org.idea.mq.redis.framework.mq.xstream.RedisStreamMQListener;
import org.idea.mq.redis.framework.redis.IRedisService;
import redis.clients.jedis.StreamEntry;

import javax.annotation.Resource;
import java.util.Map;

import static org.idea.mq.redis.framework.config.MQConstants.SUCCESS;

/**
 * @Author linhao
 * @Date created in 10:07 下午 2022/2/9
 */
@StreamListener(streamName = "order-service:order-payed-topic", groupName = "order-service-group", consumerName = "user-service-consumer")
public class OrderPayedListener implements RedisStreamMQListener {

    @Resource
    private IRedisService iRedisService;

    @Override
    public HandlerResult handleMsg(StreamEntry streamEntry) {
        Map<String, String> map = streamEntry.getFields();
        String json = map.get("json");
        System.out.println("payMsg is : " + json);
        return SUCCESS;
    }

}
