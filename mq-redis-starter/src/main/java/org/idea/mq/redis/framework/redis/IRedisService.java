package org.idea.mq.redis.framework.redis;


import redis.clients.jedis.*;

import java.util.List;
import java.util.Map;

/**
 * @author idea
 * @data 2020/4/1
 */
public interface IRedisService {

    String getStr(String key);

    Boolean setStr(String key,String value);

    long lpush(String listKey,String... value);

    String rpop(String listKey);

    List<String> brpop(String listKey);

    boolean subscribe(JedisPubSub jedisPubSub,String... channel);

    boolean pSubscribe(JedisPubSub jedisPubSub,String pattern);

    boolean publish(String channel,String content);

    boolean xAdd(String streamName, Map<String, String> stringMap);

    List<byte[]>  xRange(String streamName, String begin, String end, int limit);

    List<Map.Entry<String, List<StreamEntry>>>  xBlockRead(String streamName);

    String xGroup(String streamName, String groupName);

    List<Map.Entry<String, List<StreamEntry>>> xreadGroup(String streamName, String groupName, String consumerName);

    List<Map.Entry<String, List<StreamEntry>>> xreadGroup(String streamName, String groupName, StreamEntryID streamEntryID, int count, String consumerName);

    long xreadGroup(String streamName, String groupName, StreamEntryID streamEntryID);

    List<StreamPendingEntry> xpending(String streamName, String groupName, StreamEntryID start, int count);

    List<StreamConsumersInfo> xInfo(String streamName, String groupName);

    long xack(String streamName, String groupName, StreamEntryID streamEntryID);
}
