package org.idea.mq.framework.rocketmq.consumer;

import org.apache.rocketmq.client.consumer.rebalance.AllocateMessageQueueAveragely;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author linhao
 * @Date created in 8:40 上午 2022/4/19
 */
public class AlwaysFirstQueueAveragely extends AllocateMessageQueueAveragely {

    @Override
    public List<MessageQueue> allocate(String consumerGroup, String currentCID, List<MessageQueue> mqAll, List<String> cidAll) {
        mqAll = mqAll.subList(0,1);
        return super.allocate(consumerGroup, currentCID, mqAll, cidAll);
    }

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        List<String> subList = list.subList(0,0);
        System.out.println(subList);
    }
}
