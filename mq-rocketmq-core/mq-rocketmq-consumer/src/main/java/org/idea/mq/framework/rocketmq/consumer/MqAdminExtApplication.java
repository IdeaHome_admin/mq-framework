package org.idea.mq.framework.rocketmq.consumer;

import org.apache.rocketmq.client.ClientConfig;
import org.apache.rocketmq.client.MQAdmin;
import org.apache.rocketmq.client.impl.MQAdminImpl;
import org.apache.rocketmq.client.impl.factory.MQClientInstance;

/**
 * @Author linhao
 * @Date created in 9:01 上午 2022/4/19
 */
public class MqAdminExtApplication {

    public static final String NAME_SERVER = "localhost:9876";


    public static void main(String[] args) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setNamesrvAddr(NAME_SERVER);
    }
}
