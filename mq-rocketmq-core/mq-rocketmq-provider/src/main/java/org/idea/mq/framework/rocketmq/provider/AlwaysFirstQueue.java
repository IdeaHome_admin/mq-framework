package org.idea.mq.framework.rocketmq.provider;

import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;

import java.util.Arrays;
import java.util.List;

/**
 * 永远选择第一条队列
 *
 * @Author linhao
 * @Date created in 10:31 下午 2022/4/18
 */
public class AlwaysFirstQueue implements MessageQueueSelector {

    @Override
    public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
        System.out.println(Arrays.asList(mqs));
        return mqs.get(0);
    }

}
