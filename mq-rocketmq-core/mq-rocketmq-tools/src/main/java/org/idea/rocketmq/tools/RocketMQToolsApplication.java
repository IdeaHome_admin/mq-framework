package org.idea.rocketmq.tools;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.admin.TopicStatsTable;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.apache.rocketmq.tools.admin.DefaultMQAdminExt;

/**
 * @Author linhao
 * @Date created in 11:12 上午 2022/4/19
 */
public class RocketMQToolsApplication {

    public static final String NAME_SERVER = "localhost:9876";

    public static void main(String[] args) throws MQClientException, InterruptedException, RemotingException, MQBrokerException {
        DefaultMQAdminExt defaultMQAdminExt = new DefaultMQAdminExt();
        defaultMQAdminExt.setAdminExtGroup("test-group");
        defaultMQAdminExt.setNamesrvAddr(NAME_SERVER);
        defaultMQAdminExt.start();
        System.out.println("启动了nameserver admin");
        String newTopic = defaultMQAdminExt.getAdminExtGroup();
        String brokerName = "rocketmq";
        defaultMQAdminExt.createTopic(brokerName, newTopic, 1);
        // 稍等片刻
        Thread.sleep(3000);
        TopicStatsTable topicStatsTable = defaultMQAdminExt.examineTopicStats(newTopic);
        if(topicStatsTable!=null){
            System.out.println(JSON.toJSON(topicStatsTable));
        } else {
            System.out.println("topic 不存在！");
        }

    }
}
