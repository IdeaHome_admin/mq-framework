package org.idea.mq.framework.rocketmq.common;

/**
 * @Author linhao
 * @Date created in 8:55 上午 2022/2/20
 */
public interface MqMsgSerializer {

    /**
     * 将消息进行序列化传递
     *
     * @param object
     * @param <T>
     * @return
     */
    <T> byte[] serializer(T object);


    /**
     * 反序列化操作
     *
     * @param data
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T unSerializer(byte[] data,Class<T> clazz);
}
