package org.idea.mq.framework.rocketmq.common;


import com.alibaba.fastjson.JSON;

public class FastJsonSerializer implements MqMsgSerializer {

    @Override
    public <T> byte[] serializer(T object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    public <T> T unSerializer(byte[] data, Class<T> clazz) {
        return JSON.parseObject(data, clazz);
    }

}
