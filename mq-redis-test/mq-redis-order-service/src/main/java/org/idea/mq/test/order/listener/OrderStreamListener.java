package org.idea.mq.test.order.listener;

import org.idea.mq.redis.framework.bean.HandlerResult;
import org.idea.mq.redis.framework.config.StreamListener;
import org.idea.mq.redis.framework.mq.xstream.RedisStreamMQListener;
import redis.clients.jedis.StreamEntry;

import static org.idea.mq.redis.framework.config.MQConstants.SUCCESS;

/**
 * @Author linhao
 * @Date created in 3:07 下午 2022/2/10
 */
@StreamListener(streamName = "order-payed-stream", groupName = "order-service-group", consumerName = "order-service-consumer")
public class OrderStreamListener implements RedisStreamMQListener {

    @Override
    public HandlerResult handleMsg(StreamEntry streamEntry) {
        System.out.println("order-service:" + streamEntry);
        return SUCCESS;
    }
}
