package org.idea.mq.test.order.controller;

import org.idea.mq.redis.framework.producer.IStreamProducer;
import org.idea.mq.redis.framework.redis.IRedisService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;

/**
 * @Author linhao
 * @Date created in 3:35 下午 2022/2/10
 */
@RestController
@RequestMapping(value = "/order/test")
public class TestController {

    @Resource
    private IRedisService iRedisService;
    @Resource
    private IStreamProducer streamProducer;

    private static final String USER_STREAM = "user-upgrade-stream";

    @GetMapping(value = "/create-stream")
    public void createStream(String streamName, String groupName) {
        iRedisService.xGroup(streamName, groupName);
    }

    @GetMapping(value = "/send-mq")
    public void sendMq() {
        streamProducer.sendMsg(USER_STREAM, "{\"userId\":" + new Random().nextInt(1293979112) + "}");
    }
}
