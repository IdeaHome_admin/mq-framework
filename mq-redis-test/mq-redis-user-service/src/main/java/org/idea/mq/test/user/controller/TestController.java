package org.idea.mq.test.user.controller;

import org.idea.mq.redis.framework.producer.IStreamProducer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;

/**
 * @Author linhao
 * @Date created in 3:41 下午 2022/2/10
 */
@RestController
@RequestMapping("/user/test")
public class TestController {
    @Resource
    private IStreamProducer iStreamProducer;

    private static final String ORDER_STREAM = "order-payed-stream";


    @GetMapping(value = "/send-mq")
    public void sendMq(){
        iStreamProducer.sendMsg(ORDER_STREAM,"{\"orderId\":"+new Random().nextInt(1293979112) +"}");
    }
}
