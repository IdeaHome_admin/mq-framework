package org.idea.mq.test.user.listener;

import org.idea.mq.redis.framework.bean.HandlerResult;
import org.idea.mq.redis.framework.config.StreamListener;
import org.idea.mq.redis.framework.mq.xstream.RedisStreamMQListener;
import redis.clients.jedis.StreamEntry;

import static org.idea.mq.redis.framework.config.MQConstants.SUCCESS;

/**
 * @Author linhao
 * @Date created in 3:42 下午 2022/2/10
 */
@StreamListener(streamName = "user-upgrade-stream", groupName = "user-service-group-03", consumerName = "user-service-consumer")
public class UserStreamListener implements RedisStreamMQListener {

    @Override
    public HandlerResult handleMsg(StreamEntry streamEntry) {
        System.out.println("user-service:" + streamEntry);
        return SUCCESS;
    }
}
